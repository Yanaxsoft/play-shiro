import sbt.DefaultMavenRepository

resolvers += DefaultMavenRepository

// The Typesafe repository
resolvers += "Typesafe repository" at "https://dl.bintray.com/typesafe/maven-releases/"

// Use the Play sbt plugin for Play projects
addSbtPlugin("com.typesafe.play" % "sbt-plugin" % "2.5.1")

// Generates getters and setters for Java properties
addSbtPlugin("com.typesafe.sbt" % "sbt-play-enhancer" % "1.1.0")

// Plugin for publishing to bintray
addSbtPlugin("me.lessis" % "bintray-sbt" % "0.3.0")
