Apache Shiro Integration
========================

Example SBT Configuration
-------------------------

`<project root>/build.sbt`

    ...

    // Enable Shiro's annotation-based authorization with AspectJ
    aspectjSettings ++ Seq(
        binaries in Aspectj <++= update map { report =>
            report.matching(moduleFilter(organization = "ch.insign", name = "play-shiro*"))
        },
        inputs in Aspectj <+= compiledClasses,
        products in Compile <<= products in Aspectj,
        products in Runtime <<= products in Compile,
        fullClasspath in Runtime <<= useInstrumentedClasses(Runtime)
    )

    // Load play-shiro as a subproject
    lazy val playShiro = project.in(file("modules/play-shiro"))
    
    lazy val root = project.in(file("."))
        .aggregate(playShiro) // recompile playShiro with the root project
        .dependsOn(playShiro) // add playShiro to the root project classpath

