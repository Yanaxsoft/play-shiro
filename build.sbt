name := "play-shiro"

organization := "ch.insign"

version := "1.3.0"

scalaVersion := "2.11.7"

libraryDependencies ++= Seq(
  "org.apache.shiro" % "shiro-core" % "1.2.2",
  "javax.enterprise" % "cdi-api" % "1.1"
)

// Change max-filename-length for scala-Compiler. Longer filenames (not sure what the threshold is) causes problems
// with encrypted home directories under ubuntu
scalacOptions ++= Seq("-Xmax-classfile-name", "100")

licenses += ("Apache-2.0", url("https://www.apache.org/licenses/LICENSE-2.0.html"))
bintrayRepository := "play-cms"
bintrayOrganization := Some("insign")
publishMavenStyle := true

lazy val shiro = (project in file("."))
  .enablePlugins(PlayJava)
