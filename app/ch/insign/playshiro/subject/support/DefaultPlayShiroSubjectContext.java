package ch.insign.playshiro.subject.support;

import org.slf4j.LoggerFactory;
import org.slf4j.Logger;

import org.apache.shiro.subject.Subject;
import org.apache.shiro.subject.support.DefaultSubjectContext;

import play.mvc.Http.Context;

import ch.insign.playshiro.subject.PlayShiroSubject;
import ch.insign.playshiro.subject.PlayShiroSubjectContext;

public class DefaultPlayShiroSubjectContext extends DefaultSubjectContext implements
PlayShiroSubjectContext {

    private static final long serialVersionUID = 1896848921742292018L;

    private static final String HTTP_CONTEXT = DefaultPlayShiroSubjectContext.class.getName() + ".HTTP_CONTEXT";

    public DefaultPlayShiroSubjectContext() {
    }

    public DefaultPlayShiroSubjectContext(PlayShiroSubjectContext context) {
        super(context);
    }

    @Override
    public String resolveHost() {
        String host = super.resolveHost();
        if (host == null) {
            Context ctx = resolveHttpContext();
            if (ctx != null) {
                host = ctx.request().host();
            }
        }
        return host;
    }

    @Override
    public Context getHttpContext() {
        return getTypedValue(HTTP_CONTEXT, Context.class);
    }

    @Override
    public void setHttpContext(Context context) {
        if (context != null) {
            put(HTTP_CONTEXT, context);
        }
    }

    @Override
    public Context resolveHttpContext() {
        Context context = getHttpContext();

        // fall back on existing subject instance if it exists:
        if (context == null) {
            Subject existing = getSubject();
            if (existing instanceof PlayShiroSubject) {
                context = ((PlayShiroSubject) existing).getHttpContext();
            }
        }

        return context;
    }

}
