package ch.insign.playshiro.subject.support;

import org.slf4j.LoggerFactory;
import org.slf4j.Logger;

import org.apache.shiro.mgt.SecurityManager;
import org.apache.shiro.session.Session;
import org.apache.shiro.session.mgt.SessionContext;
import org.apache.shiro.subject.PrincipalCollection;
import org.apache.shiro.subject.support.DelegatingSubject;
import org.apache.shiro.util.StringUtils;

import play.mvc.Http;

import ch.insign.playshiro.session.mgt.DefaultPlayShiroSessionContext;
import ch.insign.playshiro.subject.PlayShiroSubject;

public class PlayShiroDelegatingSubject extends DelegatingSubject implements PlayShiroSubject  {
	private final static Logger logger = LoggerFactory.getLogger(PlayShiroDelegatingSubject.class);

    private final Http.Context context;

    public PlayShiroDelegatingSubject(PrincipalCollection principals, boolean authenticated,
            String host, Session session, Http.Context context,
            SecurityManager securityManager) {
        this(principals, authenticated, host, session, true, context, securityManager);
    }

    public PlayShiroDelegatingSubject(PrincipalCollection principals, boolean authenticated,
            String host, Session session, boolean sessionEnabled,
            Http.Context context,
            SecurityManager securityManager) {
        super(principals, authenticated, host, session, sessionEnabled, securityManager);
        this.context = context;
    }

    @Override
    public Http.Context getHttpContext() {
        return context;
    }

    @Override
    protected SessionContext createSessionContext() {
        DefaultPlayShiroSessionContext sc = new DefaultPlayShiroSessionContext();
        String host = getHost();
        if (StringUtils.hasText(host)) {
            sc.setHost(host);
        }
        sc.setHttpContext(context);
        return sc;
    }
}
