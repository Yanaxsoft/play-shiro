package ch.insign.playshiro.subject;

import org.slf4j.LoggerFactory;
import org.slf4j.Logger;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.mgt.SecurityManager;
import org.apache.shiro.subject.Subject;
import org.apache.shiro.subject.SubjectContext;

import play.mvc.Http;

import ch.insign.playshiro.subject.support.DefaultPlayShiroSubjectContext;
import ch.insign.playshiro.util.HttpContextSource;

public interface PlayShiroSubject extends Subject, HttpContextSource {

    public static class Builder extends Subject.Builder  {
	private final static Logger logger = LoggerFactory.getLogger(Builder.class);

        public Builder() {
            super(SecurityUtils.getSecurityManager());
        }

        public Builder(SecurityManager securityManager) {
            super(securityManager);
        }

        public Builder(SecurityManager securityManager, Http.Context context) {
            super(securityManager);
            httpContext(context);
        }

        @Override
        protected SubjectContext newSubjectContextInstance() {
            return new DefaultPlayShiroSubjectContext();
        }

        public Builder httpContext(Http.Context context) {
            if (context != null) {
                ((PlayShiroSubjectContext) getSubjectContext()).setHttpContext(context);
            }
            return this;
        }

        public PlayShiroSubject buildPlayShiroSubject() {
            Subject subject = super.buildSubject();
            if (!(subject instanceof PlayShiroSubject)) {
                String msg = "Subject implementation returned from the SecurityManager was not a " +
                        PlayShiroSubject.class.getName() + " implementation.  Please ensure a Play!-enabled SecurityManager " +
                        "has been configured and made available to this builder.";
                throw new IllegalStateException(msg);
            }
            return (PlayShiroSubject) subject;
        }
    }
}
