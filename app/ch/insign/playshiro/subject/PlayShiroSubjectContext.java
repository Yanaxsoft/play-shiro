package ch.insign.playshiro.subject;

import org.slf4j.LoggerFactory;
import org.slf4j.Logger;

import org.apache.shiro.subject.SubjectContext;

import play.mvc.Http;

import ch.insign.playshiro.util.HttpContextSource;

public interface PlayShiroSubjectContext extends HttpContextSource, SubjectContext {

    @Override
    Http.Context getHttpContext();

    void setHttpContext(Http.Context context);

    Http.Context resolveHttpContext();

}
