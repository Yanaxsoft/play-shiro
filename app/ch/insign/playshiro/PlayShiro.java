package ch.insign.playshiro;

import ch.insign.playshiro.env.PlayShiroEnvironment;
import ch.insign.playshiro.env.PlayShiroEnvironmentLoader;
import ch.insign.playshiro.env.PlayShiroEnvironmentLoaderFactory;
import com.google.inject.Inject;
import org.apache.shiro.util.LifecycleUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import play.Application;
import play.api.inject.ApplicationLifecycle;

import java.util.concurrent.CompletableFuture;

public class PlayShiro {

    private final static Logger logger = LoggerFactory.getLogger(PlayShiro.class);

    private static final String ENVIRONMENT_LOADER_CLASS_PARAM = "playshiro.environmentLoaderClass";
    private static final String PLAYSHIRO_PLUGIN_ENABLED_PARAM = "playshiro.enabled";

    private final Application application;
    private final PlayShiroEnvironmentLoader environmentLoader;
    private PlayShiroEnvironment environment;

    @Inject
    public PlayShiro(Application app, ApplicationLifecycle lifecycle) {

        application = app;

        environmentLoader = PlayShiroEnvironmentLoaderFactory.create(
                application.configuration().getString(ENVIRONMENT_LOADER_CLASS_PARAM));

        environment = environmentLoader.initEnvironment(application);

        logger.info("Initializing PlayShiroEnvironment.");

        lifecycle.addStopHook(() -> {
            logger.info("Cleaning up PlayShiroEnvironment.");

            LifecycleUtils.destroy(environment);
            environment = null;

            return CompletableFuture.completedFuture(null);
        });
    }

    public PlayShiroEnvironment getEnvironment() {
        return environment;
    }

    public boolean enabled() {
        return application.configuration().getBoolean(PLAYSHIRO_PLUGIN_ENABLED_PARAM, true);
    }

}
