package ch.insign.playshiro.env;

import ch.insign.playshiro.mgt.PlayShiroSecurityManager;
import org.apache.shiro.env.DefaultEnvironment;
import org.apache.shiro.mgt.SecurityManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class DefaultPlayShiroEnvironment extends DefaultEnvironment implements MutablePlayShiroEnvironment  {
	private final static Logger logger = LoggerFactory.getLogger(DefaultPlayShiroEnvironment.class);

    public DefaultPlayShiroEnvironment() {
        super();
    }

    @Override
    public SecurityManager getSecurityManager() throws IllegalStateException {
        return getPlayShiroSecurityManager();
    }

    @Override
    public void setSecurityManager(SecurityManager securityManager) {
        assertPlayShiroSecurityManager(securityManager);
        super.setSecurityManager(securityManager);
    }

    @Override
    public PlayShiroSecurityManager getPlayShiroSecurityManager() {
        SecurityManager securityManager = super.getSecurityManager();
        assertPlayShiroSecurityManager(securityManager);
        return (PlayShiroSecurityManager) securityManager;
    }

    @Override
    public void setPlayShiroSecurityManager(PlayShiroSecurityManager playShiroSecurityManager) {
        super.setSecurityManager(playShiroSecurityManager);
    }

    private void assertPlayShiroSecurityManager(SecurityManager sm) {
        if (!(sm instanceof PlayShiroSecurityManager)) {
            throw new IllegalStateException(
                    "SecurityManager instance must be a " + PlayShiroSecurityManager.class.getName() + " instance.");
        }
    }
}
