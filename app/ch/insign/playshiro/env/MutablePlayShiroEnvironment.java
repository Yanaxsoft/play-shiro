package ch.insign.playshiro.env;

import ch.insign.playshiro.mgt.PlayShiroSecurityManager;

public interface MutablePlayShiroEnvironment extends PlayShiroEnvironment {
    void setPlayShiroSecurityManager(PlayShiroSecurityManager playShiroSecurityManager);
}
