package ch.insign.playshiro.env;

import org.slf4j.LoggerFactory;
import org.slf4j.Logger;

import org.apache.shiro.config.ConfigurationException;
import org.apache.shiro.util.ClassUtils;
import org.apache.shiro.util.UnknownClassException;

public class PlayShiroEnvironmentLoaderFactory  {
	private final static Logger logger = LoggerFactory.getLogger(PlayShiroEnvironmentLoaderFactory.class);

    public static PlayShiroEnvironmentLoader create(String className) {

        Class<?> clazz = DefaultPlayShiroEnvironmentLoader.class;

        if (className != null) {
            try {
                clazz = ClassUtils.forName(className);
            } catch (UnknownClassException ex) {
                throw new ConfigurationException(
                        "Failed to load custom PlayShiroEnvironmentLoader class [" + className + "]", ex);
            }
        }

        if (!PlayShiroEnvironmentLoader.class.isAssignableFrom(clazz)) {
            throw new ConfigurationException("Custom PlayShiroEnvironmentLoader class [" + clazz.getName() +
                    "] is not of required type [" + PlayShiroEnvironmentLoader.class.getName() + "]");
        }

        PlayShiroEnvironmentLoader loader = (PlayShiroEnvironmentLoader) ClassUtils.newInstance(clazz);

        return loader;
    }
}
