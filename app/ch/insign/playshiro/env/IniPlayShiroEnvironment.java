package ch.insign.playshiro.env;

import org.slf4j.LoggerFactory;
import org.slf4j.Logger;

import java.io.IOException;
import java.io.InputStream;
import java.util.Map;

import org.apache.shiro.ShiroException;
import org.apache.shiro.config.ConfigurationException;
import org.apache.shiro.config.Ini;
import org.apache.shiro.config.IniFactorySupport;
import org.apache.shiro.io.ResourceUtils;
import org.apache.shiro.util.CollectionUtils;
import org.apache.shiro.util.Destroyable;
import org.apache.shiro.util.Initializable;
import org.apache.shiro.util.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.insign.playshiro.config.PlayShiroIniSecurityManagerFactory;
import ch.insign.playshiro.mgt.PlayShiroSecurityManager;

public class IniPlayShiroEnvironment extends ResourceBasedPlayShiroEnvironment implements Initializable, Destroyable  {
	private final static Logger logger = LoggerFactory.getLogger(IniPlayShiroEnvironment.class);

    public static final String DEFAULT_PLAY_INI_RESOURCE_PATH = "/conf/shiro.ini";

    private Ini ini;

    public Ini getIni() {
        return this.ini;
    }

    public void setIni(Ini ini) {
        this.ini = ini;
    }

    @Override
    public void init() throws ShiroException {
        Ini ini = getIni();

        String[] configLocations = getConfigLocations();

        if (logger.isWarnEnabled() && !CollectionUtils.isEmpty(ini) &&
                configLocations != null && configLocations.length > 0) {
            logger.warn("Explicit INI instance has been provided, but configuration locations have also been "
                    + "specified.  The {} implementation does not currently support multiple Ini config, but this may "
                    + "be supported in the future. Only the INI instance will be used for configuration.",
                    IniPlayShiroEnvironment.class.getName());
        }

        if (CollectionUtils.isEmpty(ini)) {
            logger.debug("Checking any specified config locations.");
            ini = getSpecifiedIni(configLocations);
        }

        if (CollectionUtils.isEmpty(ini)) {
            logger.debug("No INI instance or config locations specified.  Trying default config locations.");
            ini = getDefaultIni();
        }

        if (CollectionUtils.isEmpty(ini)) {
            String msg = "Shiro INI configuration was either not found or discovered to be empty/unconfigured.";
            throw new ConfigurationException(msg);
        }

        setIni(ini);

        configure();
    }

    protected void configure() {

        this.objects.clear();

        PlayShiroSecurityManager securityManager = createPlayShiroSecurityManager();
        setPlayShiroSecurityManager(securityManager);
    }

    protected Ini getSpecifiedIni(String[] configLocations) throws ConfigurationException {

        Ini ini = null;

        if (configLocations != null && configLocations.length > 0) {

            if (configLocations.length > 1) {
                logger.warn("More than one Shiro .ini config location has been specified.  Only the first will be " +
                        "used for configuration as the {} implementation does not currently support multiple " +
                        "files.  This may be supported in the future however.", IniPlayShiroEnvironment.class.getName());
            }

            // required, as it is user specified:
            ini = createIni(configLocations[0], true);
        }

        return ini;
    }

    protected Ini getDefaultIni() {

        Ini ini = null;

        String[] configLocations = getDefaultConfigLocations();
        if (configLocations != null) {
            for (String location : configLocations) {
                ini = createIni(location, false);
                if (!CollectionUtils.isEmpty(ini)) {
                    logger.debug("Discovered non-empty INI configuration at location '{}'.  Using for configuration.",
                            location);
                    break;
                }
            }
        }

        return ini;
    }

    protected Ini createIni(String configLocation, boolean required) throws ConfigurationException {

        Ini ini = null;

        if (configLocation != null) {
            ini = convertPathToIni(configLocation, required);
        }
        if (required && CollectionUtils.isEmpty(ini)) {
            String msg = "Required configuration location '" + configLocation + "' does not exist or did not " +
                    "contain any INI configuration.";
            throw new ConfigurationException(msg);
        }

        return ini;
    }

    protected PlayShiroSecurityManager createPlayShiroSecurityManager() {
        PlayShiroIniSecurityManagerFactory factory;
        Ini ini = getIni();
        if (CollectionUtils.isEmpty(ini)) {
            factory = new PlayShiroIniSecurityManagerFactory();
        } else {
            factory = new PlayShiroIniSecurityManagerFactory(ini);
        }

        PlayShiroSecurityManager pssm = (PlayShiroSecurityManager) factory.getInstance();

        // SHIRO-306 - get beans after they've been created (the call was before
        // the factory.getInstance() call,
        // which always returned null.
        Map<String, ?> beans = factory.getBeans();
        if (!CollectionUtils.isEmpty(beans)) {
            this.objects.putAll(beans);
        }

        return pssm;
    }

    protected String[] getDefaultConfigLocations() {
        return new String[] {
                DEFAULT_PLAY_INI_RESOURCE_PATH,
                IniFactorySupport.DEFAULT_INI_RESOURCE_PATH
        };
    }

    private Ini convertPathToIni(String path, boolean required) {
        Ini ini = null;

        if (StringUtils.hasText(path)) {
            InputStream is = null;

            try {
                is = ResourceUtils.getInputStreamForPath(path);
            } catch (IOException e) {
                if (required) {
                    throw new ConfigurationException(e);
                } else {
                    if (logger.isDebugEnabled()) {
                        logger.debug("Unable to load optional path '" + path + "'.", e);
                    }
                }
            }

            if (is != null) {
                ini = new Ini();
                ini.load(is);
            } else {
                if (required) {
                    throw new ConfigurationException("Unable to load resource path '" + path + "'");
                }
            }
        }

        return ini;
    }
}
