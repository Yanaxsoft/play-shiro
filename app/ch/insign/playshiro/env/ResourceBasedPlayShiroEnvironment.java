package ch.insign.playshiro.env;

import org.slf4j.LoggerFactory;
import org.slf4j.Logger;

import org.apache.shiro.config.ResourceConfigurable;
import org.apache.shiro.util.StringUtils;

public class ResourceBasedPlayShiroEnvironment extends DefaultPlayShiroEnvironment implements ResourceConfigurable  {
	private final static Logger logger = LoggerFactory.getLogger(ResourceBasedPlayShiroEnvironment.class);

    private String[] configLocations;

    public String[] getConfigLocations() {
        return configLocations;
    }

    @Override
    public void setConfigLocations(String locations) {
        if (!StringUtils.hasText(locations)) {
            throw new IllegalArgumentException("Null/empty locations argument not allowed.");
        }
        String[] arr = StringUtils.split(locations);
        setConfigLocations(arr);
    }

    @Override
    public void setConfigLocations(String[] locations) {
        configLocations = locations;
    }

}
