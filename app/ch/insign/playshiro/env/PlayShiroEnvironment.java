package ch.insign.playshiro.env;

import ch.insign.playshiro.mgt.PlayShiroSecurityManager;
import org.apache.shiro.env.Environment;

public interface PlayShiroEnvironment extends Environment {
    PlayShiroSecurityManager getPlayShiroSecurityManager();
}
