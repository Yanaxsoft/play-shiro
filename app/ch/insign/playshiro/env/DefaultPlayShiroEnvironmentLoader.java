package ch.insign.playshiro.env;

import org.apache.shiro.config.ConfigurationException;
import org.apache.shiro.config.ResourceConfigurable;
import org.apache.shiro.util.ClassUtils;
import org.apache.shiro.util.LifecycleUtils;
import org.apache.shiro.util.StringUtils;
import org.apache.shiro.util.UnknownClassException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import play.Application;

public class DefaultPlayShiroEnvironmentLoader implements PlayShiroEnvironmentLoader  {
    private static final Logger logger = LoggerFactory.getLogger(DefaultPlayShiroEnvironmentLoader.class);

    public static final String ENVIRONMENT_CLASS_PARAM = "playshiro.environmentClass";
    public static final String CONFIG_LOCATIONS_PARAM = "playshiro.configLocations";

    @Override
    public PlayShiroEnvironment initEnvironment(Application application) throws IllegalStateException {
        logger.info("Starting PlayShiroEnvironment initialization.");

        long startTime = System.currentTimeMillis();

        try {
            PlayShiroEnvironment environment = createEnvironment(application);

            if (logger.isInfoEnabled()) {
                long elapsed = System.currentTimeMillis() - startTime;
                logger.info("PlayShiroEnvironment initialized in {} ms.", elapsed);
            }

            return environment;
        } catch (RuntimeException ex) {
            logger.error("PlayShiroEnvironment initialization failed", ex);
            throw ex;
        } catch (Error err) {
            logger.error("PlayShiroEnvironment initialization failed", err);
            throw err;
        }
    }

    protected PlayShiroEnvironment createEnvironment(Application application) {
        Class<?> clazz = determinePlayShiroEnvironmentClass(application);
        if (!MutablePlayShiroEnvironment.class.isAssignableFrom(clazz)) {
            throw new ConfigurationException("Custom PlayShiroEnvironment class [" + clazz.getName() +
                    "] is not of required type [" + PlayShiroEnvironment.class.getName() + "]");
        }

        String configLocations = application.configuration().getString(CONFIG_LOCATIONS_PARAM);
        boolean configSpecified = StringUtils.hasText(configLocations);

        if (configSpecified && !(ResourceConfigurable.class.isAssignableFrom(clazz))) {
            String msg = "PlayShiroEnvironment class [" + clazz.getName() + "] does not implement the " +
                    ResourceConfigurable.class.getName() + "interface.  This is required to accept any " +
                    "configured " + CONFIG_LOCATIONS_PARAM + "value(s).";
            throw new ConfigurationException(msg);
        }

        MutablePlayShiroEnvironment environment = (MutablePlayShiroEnvironment) ClassUtils.newInstance(clazz);

        if (configSpecified && (environment instanceof ResourceConfigurable)) {
            ((ResourceConfigurable) environment).setConfigLocations(configLocations);
        }

        customizeEnvironment(environment);

        LifecycleUtils.init(environment);

        return environment;
    }

    protected Class<?> determinePlayShiroEnvironmentClass(Application application) {
        String className = application.configuration().getString(ENVIRONMENT_CLASS_PARAM);
        if (className != null) {
            try {
                return ClassUtils.forName(className);
            } catch (UnknownClassException ex) {
                throw new ConfigurationException(
                        "Failed to load custom PlayShiroEnvironment class [" + className + "]", ex);
            }
        } else {
            return IniPlayShiroEnvironment.class;
        }
    }

    protected void customizeEnvironment(PlayShiroEnvironment environment) {
    }
}
