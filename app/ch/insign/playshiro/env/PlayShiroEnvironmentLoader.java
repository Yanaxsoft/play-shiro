package ch.insign.playshiro.env;

import org.slf4j.LoggerFactory;
import org.slf4j.Logger;

import play.Application;

public interface PlayShiroEnvironmentLoader {
    PlayShiroEnvironment initEnvironment(Application application);
}
