package ch.insign.playshiro.util;

import org.slf4j.LoggerFactory;
import org.slf4j.Logger;

import play.mvc.Http;

public class HttpContextUtils  {
	private final static Logger logger = LoggerFactory.getLogger(HttpContextUtils.class);

    public static boolean isHttpContextSource(Object obj) {
        return obj instanceof HttpContextSource;
    }

    public static boolean hasHttpContext(Object obj) {
        return isHttpContextSource(obj)
                && ((HttpContextSource) obj).getHttpContext() != null;
    }

    public static Http.Context getHttpContext(Object obj) {
        if (hasHttpContext(obj)) {
            return ((HttpContextSource) obj).getHttpContext();
        }

        return null;
    }
}
