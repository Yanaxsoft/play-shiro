package ch.insign.playshiro.util;

import ch.insign.playshiro.PlayShiro;
import ch.insign.playshiro.env.PlayShiroEnvironment;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import play.Application;
import play.Play;

public class PluginUtils  {
	private final static Logger logger = LoggerFactory.getLogger(PluginUtils.class);

    public static PlayShiroEnvironment getPlayShiroEnvironment(Application application) {
        return application.injector().instanceOf(PlayShiro.class).getEnvironment();
    }

    public static PlayShiroEnvironment getPlayShiroEnvironment() {
        return getPlayShiroEnvironment(Play.application());
    }

}
