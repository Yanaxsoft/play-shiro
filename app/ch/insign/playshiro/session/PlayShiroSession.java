package ch.insign.playshiro.session;

import org.slf4j.LoggerFactory;
import org.slf4j.Logger;

import org.apache.shiro.codec.Base64;
import org.apache.shiro.io.DefaultSerializer;
import org.apache.shiro.io.SerializationException;
import org.apache.shiro.io.Serializer;
import org.apache.shiro.session.InvalidSessionException;
import org.apache.shiro.session.Session;
import org.apache.shiro.util.StringUtils;
import play.mvc.Http;

import java.io.Serializable;
import java.text.DateFormat;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;

public class PlayShiroSession implements Session  {
	private final static Logger logger = LoggerFactory.getLogger(PlayShiroSession.class);

    private static final String SESSION_ID_KEY = "_sid";
    private static final String START_TIMESTAMP_KEY = "_startTimestamp";
    private static final String LAST_ACCESS_TIME_KEY = "_lastAccessTime";
    private static final String TIMEOUT_KEY = "_timeout";
    private static final String HOST_KEY = "_host";

    private static final Serializer<Object> serializer = new DefaultSerializer<>();

    private Http.Session session = null;

    public PlayShiroSession(Http.Session playSession, String host) {
        if (playSession == null) {
            String msg = "PlaySession constructor argument cannot be null.";
            throw new IllegalArgumentException(msg);
        }

        session = playSession;
        if (StringUtils.hasText(host)) {
            session.put(HOST_KEY, host);
        }

        if (getId(session) == null) {
            long timestamp = System.currentTimeMillis() / 1000;
            session.put(START_TIMESTAMP_KEY, String.valueOf(timestamp));
            session.put(SESSION_ID_KEY, generateId());
        }
    }

    @Override
    public Serializable getId() {
        return getId(session);
    }

    public static Serializable getId(Http.Session playSession) {
        return playSession.get(SESSION_ID_KEY);
    }

    public static String generateId() {
        return java.util.UUID.randomUUID().toString();
    }

    @Override
    public Date getStartTimestamp() {
        try {
            return DateFormat.getDateInstance().parse(session.get(START_TIMESTAMP_KEY));
        } catch (ParseException e) {
            return new Date();
        }
    }

    @Override
    public Date getLastAccessTime() {
        try {
            return DateFormat.getDateInstance().parse(session.get(LAST_ACCESS_TIME_KEY));
        } catch (ParseException e) {
            return new Date();
        }
    }

    @Override
    public long getTimeout() throws InvalidSessionException {
        try {
            return Long.parseLong(session.get(TIMEOUT_KEY));
        } catch (NumberFormatException e) {
            return 0;
        }
    }

    @Override
    public void setTimeout(long maxIdleTimeInMillis)
            throws InvalidSessionException {
        session.put(TIMEOUT_KEY, Long.toString(maxIdleTimeInMillis));
    }

    public void setHost(String host) {
        session.put(HOST_KEY, host);
    }

    @Override
    public String getHost() {
        return session.get(HOST_KEY);
    }

    @Override
    public void touch() throws InvalidSessionException {
        session.put(LAST_ACCESS_TIME_KEY, new Date().toString());
    }

    @Override
    public void stop() throws InvalidSessionException {
        session.clear();
    }

    @Override
    public Collection<Object> getAttributeKeys() throws InvalidSessionException {
        Collection<Object> keys = new ArrayList<>();
        try {
            keys.addAll(session.keySet());
        } catch (Exception e) {
            throw new InvalidSessionException(e);
        }
        return keys;
    }

    @Override
    public Object getAttribute(Object key) throws InvalidSessionException {
        try {
            return deserialize(session.get(assertString(key)));
        } catch (SerializationException e) {
            throw new InvalidSessionException(e);
        }
    }

    @Override
    public void setAttribute(Object key, Object value) throws InvalidSessionException {
        try {
            session.put(assertString(key), serialize(value));
        } catch (SerializationException e) {
            throw new InvalidSessionException(e);
        }
    }

    @Override
    public Object removeAttribute(Object key) throws InvalidSessionException {
        try {
            return session.remove(assertString(key));
        } catch (Exception e) {
            throw new InvalidSessionException(e);
        }
    }

    private static String assertString(Object key) {
        if (!(key instanceof String)) {
            String msg = "PlayShiroSession based implementations of the Shiro Session interface requires attribute keys "
                    + "to be String objects.  The PlayShiroSession class does not support anything other than String keys.";
            throw new IllegalArgumentException(msg);
        }
        return (String) key;
    }

    private Object deserialize(String base64) throws SerializationException {
        if (null == base64) {
            return null;
        }

        byte[] serialized = Base64.decode(ensurePadding(base64));

        return serializer.deserialize(serialized);
    }

    private String serialize(Object object) throws SerializationException {
        byte[] serialized = serializer.serialize(object);

        return Base64.encodeToString(serialized);
    }

    /**
     * Sometimes a user agent will send the cookie value without padding, most
     * likely because {@code =} is a separator in the cookie header.
     * <p/>
     * Contributed by Luis Arias. Thanks Luis!
     *
     * @param base64 the base64 encoded String that may need to be padded
     * @return the base64 String padded if necessary.
     */
    private String ensurePadding(String base64) {
        int length = base64.length();
        if (length % 4 != 0) {
            StringBuilder sb = new StringBuilder(base64);
            for (int i = 0; i < length % 4; ++i) {
                sb.append('=');
            }
            base64 = sb.toString();
        }
        return base64;
    }

}
