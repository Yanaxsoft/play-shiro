package ch.insign.playshiro.session.mgt;

import org.slf4j.LoggerFactory;
import org.slf4j.Logger;

import java.util.Map;

import org.apache.shiro.session.mgt.DefaultSessionContext;

import play.mvc.Http;


public class DefaultPlayShiroSessionContext extends DefaultSessionContext implements PlayShiroSessionContext  {
	private final static Logger logger = LoggerFactory.getLogger(DefaultPlayShiroSessionContext.class);

    private static final long serialVersionUID = 4546076386206737829L;

    private static final String HTTP_CONTEXT = DefaultPlayShiroSessionContext.class.getName() + ".HTTP_CONTEXT";

    public DefaultPlayShiroSessionContext() {
        super();
    }

    public DefaultPlayShiroSessionContext(Map<String, Object> map) {
        super(map);
    }

    @Override
    public void setHttpContext(Http.Context context) {
        if (context != null) {
            put(HTTP_CONTEXT, context);
        }
    }

    @Override
    public Http.Context getHttpContext() {
        return getTypedValue(HTTP_CONTEXT, Http.Context.class);
    }
}
