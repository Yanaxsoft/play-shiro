package ch.insign.playshiro.session.mgt;

import org.slf4j.LoggerFactory;
import org.slf4j.Logger;

import org.apache.shiro.session.mgt.SessionContext;

import play.mvc.Http;

import ch.insign.playshiro.util.HttpContextSource;

public interface PlayShiroSessionContext extends SessionContext, HttpContextSource {

    @Override
    Http.Context getHttpContext();

    void setHttpContext(Http.Context context);

}
