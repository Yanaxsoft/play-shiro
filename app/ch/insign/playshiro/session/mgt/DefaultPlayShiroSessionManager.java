package ch.insign.playshiro.session.mgt;

import org.slf4j.LoggerFactory;
import org.slf4j.Logger;

import ch.insign.playshiro.session.PlayShiroSession;
import ch.insign.playshiro.util.HttpContextSource;
import ch.insign.playshiro.util.HttpContextUtils;
import org.apache.shiro.authz.AuthorizationException;
import org.apache.shiro.session.Session;
import org.apache.shiro.session.SessionException;
import org.apache.shiro.session.mgt.DefaultSessionManager;
import org.apache.shiro.session.mgt.SessionContext;
import org.apache.shiro.session.mgt.SessionKey;
import play.mvc.Http;

import java.util.HashMap;

public class DefaultPlayShiroSessionManager extends DefaultSessionManager implements PlayShiroSessionManager  {
	private final static Logger logger = LoggerFactory.getLogger(DefaultPlayShiroSessionManager.class);

    public DefaultPlayShiroSessionManager() {
    }

    @Override
    public Session start(SessionContext context) throws AuthorizationException {
        if (!HttpContextUtils.isHttpContextSource(context)) {
            super.start(context);
        }

        return createSession((HttpContextSource) context);
    }

    @Override
    public Session getSession(SessionKey key) throws SessionException {
        if (!HttpContextUtils.isHttpContextSource(key)) {
            return super.getSession(key);
        }

        return createSession((HttpContextSource) key);
    }

    protected Session createSession(HttpContextSource httpContextSource) {
        if (HttpContextUtils.hasHttpContext(httpContextSource)) {
            Http.Context httpContext = httpContextSource.getHttpContext();
            return new PlayShiroSession(httpContext.session(), getHost(httpContextSource));
        } else {
            return new PlayShiroSession(new Http.Session(new HashMap<String, String>()), null);
        }
    }

    protected String getHost(HttpContextSource httpContextSource) {
        String host = null;

        if (httpContextSource instanceof SessionContext) {
            host = ((SessionContext) httpContextSource).getHost();
        }

        if (host == null && HttpContextUtils.hasHttpContext(httpContextSource)) {
            host = httpContextSource.getHttpContext().request().host();
        }

        return host;
    }
}
