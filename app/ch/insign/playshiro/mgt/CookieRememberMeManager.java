package ch.insign.playshiro.mgt;

import org.slf4j.LoggerFactory;
import org.slf4j.Logger;

import org.apache.shiro.codec.Base64;
import org.apache.shiro.mgt.AbstractRememberMeManager;
import org.apache.shiro.subject.Subject;
import org.apache.shiro.subject.SubjectContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import play.mvc.Http;
import play.mvc.Http.Cookie;

import ch.insign.playshiro.util.HttpContextUtils;

public class CookieRememberMeManager extends AbstractRememberMeManager  {
    private static transient final Logger logger = LoggerFactory.getLogger(CookieRememberMeManager.class);

    public static final String REMEMBER_ME_COOKIE_NAME = "rememberMe";
    public static final int REMEMBER_ME_COOKIE_MAXAGE = 60 * 60 * 24 * 365; // one
    // year

    private static final String DELETED_COOKIE_VALUE = "";

    @Override
    public void forgetIdentity(SubjectContext subjectContext) {
        if (HttpContextUtils.hasHttpContext(subjectContext)) {
            HttpContextUtils.getHttpContext(subjectContext).response()
                    .discardCookie(REMEMBER_ME_COOKIE_NAME);
        }
    }

    @Override
    protected void forgetIdentity(Subject subject) {
        if (HttpContextUtils.hasHttpContext(subject)) {
            HttpContextUtils.getHttpContext(subject).response()
                    .discardCookie(REMEMBER_ME_COOKIE_NAME);
        }
    }

    @Override
    protected void rememberSerializedIdentity(Subject subject, byte[] serialized) {
        if (!HttpContextUtils.hasHttpContext(subject)) {
            if (logger.isDebugEnabled()) {
                String msg = "Subject argument is not an HTTP-aware instance.  This is required to obtain a servlet "
                        + "request and response in order to set the rememberMe cookie. Returning immediately and "
                        + "ignoring rememberMe operation.";
                logger.debug(msg);
            }
            return;
        }

        String base64 = Base64.encodeToString(serialized);

        HttpContextUtils.getHttpContext(subject).response()
                .setCookie(REMEMBER_ME_COOKIE_NAME, base64, REMEMBER_ME_COOKIE_MAXAGE, "/", null, false, true);
    }

    @Override
    protected byte[] getRememberedSerializedIdentity(
            SubjectContext subjectContext) {

        if (!HttpContextUtils.hasHttpContext(subjectContext)) {
            if (logger.isDebugEnabled()) {
                String msg = "SubjectContext argument is not an HTTP-aware instance.  This is required to obtain a "
                        + "servlet request and response in order to retrieve the rememberMe cookie. Returning "
                        + "immediately and ignoring rememberMe operation.";
                logger.debug(msg);
            }
            return null;
        }

        Http.Context httpContext = HttpContextUtils.getHttpContext(subjectContext);

        if (isIdentityRemoved(httpContext)) {
            return null;
        }

        Cookie cookie = httpContext.request().cookie(REMEMBER_ME_COOKIE_NAME);
        String base64 = (cookie != null) ? cookie.value() : null;

        // Browsers do not always remove cookies immediately (SHIRO-183)
        // ignore cookies that are scheduled for removal
        if (DELETED_COOKIE_VALUE.equals(base64)) {
            return null;
        }

        if (base64 != null) {
            base64 = ensurePadding(base64);
            if (logger.isTraceEnabled()) {
                logger.trace("Acquired Base64 encoded identity [" + base64 + "]");
            }

            byte[] decoded = Base64.decode(base64);
            if (logger.isTraceEnabled()) {
                logger.trace("Base64 decoded byte array length: "
                        + (decoded != null ? decoded.length : 0) + " bytes.");
            }
            return decoded;
        } else {
            // no cookie set - new site visitor?
            return null;
        }
    }

    private boolean isIdentityRemoved(Http.Context context) {
        boolean removed = false;

        for (Cookie cookie : context.response().cookies()) {
            if (cookie.name().equals(REMEMBER_ME_COOKIE_NAME)) {
                removed &= cookie.maxAge() < 0;
            }
        }

        return removed;
    }

    /**
     * Sometimes a user agent will send the rememberMe cookie value without
     * padding, most likely because {@code =} is a separator in the cookie
     * header.
     * <p/>
     * Contributed by Luis Arias. Thanks Luis!
     *
     * @param base64
     *            the base64 encoded String that may need to be padded
     * @return the base64 String padded if necessary.
     */
    private String ensurePadding(String base64) {
        int length = base64.length();
        if (length % 4 != 0) {
            StringBuilder sb = new StringBuilder(base64);
            for (int i = 0; i < length % 4; ++i) {
                sb.append('=');
            }
            base64 = sb.toString();
        }
        return base64;
    }

}
