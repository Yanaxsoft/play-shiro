package ch.insign.playshiro.mgt;

import org.slf4j.LoggerFactory;
import org.slf4j.Logger;

import ch.insign.playshiro.session.mgt.DefaultPlayShiroSessionContext;
import ch.insign.playshiro.session.mgt.DefaultPlayShiroSessionManager;
import ch.insign.playshiro.session.mgt.PlayShiroSessionKey;
import ch.insign.playshiro.subject.PlayShiroSubjectContext;
import ch.insign.playshiro.subject.support.DefaultPlayShiroSubjectContext;
import ch.insign.playshiro.util.HttpContextUtils;
import org.apache.shiro.mgt.DefaultSecurityManager;
import org.apache.shiro.realm.Realm;
import org.apache.shiro.session.mgt.SessionContext;
import org.apache.shiro.session.mgt.SessionKey;
import org.apache.shiro.subject.SubjectContext;
import play.mvc.Http;

import java.io.Serializable;
import java.util.Collection;

public class DefaultPlayShiroSecurityManager extends DefaultSecurityManager implements PlayShiroSecurityManager  {
	private final static Logger logger = LoggerFactory.getLogger(DefaultPlayShiroSecurityManager.class);

    public DefaultPlayShiroSecurityManager() {
        super();
        setSubjectFactory(new DefaultPlayShiroSubjectFactory());
        setRememberMeManager(new CookieRememberMeManager());
        setSessionManager(new DefaultPlayShiroSessionManager());
    }

    public DefaultPlayShiroSecurityManager(Realm singleRealm) {
        this();
        setRealm(singleRealm);
    }

    public DefaultPlayShiroSecurityManager(Collection<Realm> realms) {
        this();
        setRealms(realms);
    }

    @Override
    protected SubjectContext createSubjectContext() {
        return new DefaultPlayShiroSubjectContext();
    }

    @Override
    protected SubjectContext copy(SubjectContext subjectContext) {
        if (subjectContext instanceof PlayShiroSubjectContext) {
            return new DefaultPlayShiroSubjectContext(
                    (PlayShiroSubjectContext) subjectContext);
        }
        return super.copy(subjectContext);
    }

    @Override
    protected SessionContext createSessionContext(SubjectContext subjectContext) {
        SessionContext sessionContext = super
                .createSessionContext(subjectContext);

        if (subjectContext instanceof PlayShiroSubjectContext) {
            PlayShiroSubjectContext sc = (PlayShiroSubjectContext) subjectContext;
            Http.Context context = sc.resolveHttpContext();
            DefaultPlayShiroSessionContext playShiroSessionContext = new DefaultPlayShiroSessionContext(sessionContext);

            if (context != null) {
                playShiroSessionContext.setHttpContext(context);
            }

            sessionContext = playShiroSessionContext;
        }

        return sessionContext;
    }

    @Override
    protected SessionKey getSessionKey(SubjectContext context) {
        if (HttpContextUtils.hasHttpContext(context)) {
            Serializable sessionId = context.getSessionId();
            Http.Context httpContext = HttpContextUtils.getHttpContext(context);
            return new PlayShiroSessionKey(sessionId, httpContext);
        } else {
            return super.getSessionKey(context);
        }
    }
}
