package ch.insign.playshiro.mgt;

import org.slf4j.LoggerFactory;
import org.slf4j.Logger;

import org.apache.shiro.mgt.DefaultSubjectFactory;
import org.apache.shiro.mgt.SecurityManager;
import org.apache.shiro.session.Session;
import org.apache.shiro.subject.PrincipalCollection;
import org.apache.shiro.subject.Subject;
import org.apache.shiro.subject.SubjectContext;

import play.mvc.Http;
import ch.insign.playshiro.subject.PlayShiroSubjectContext;
import ch.insign.playshiro.subject.support.PlayShiroDelegatingSubject;

public class DefaultPlayShiroSubjectFactory extends DefaultSubjectFactory  {
	private final static Logger logger = LoggerFactory.getLogger(DefaultPlayShiroSubjectFactory.class);

    public DefaultPlayShiroSubjectFactory() {
        super();
    }

    @Override
    public Subject createSubject(SubjectContext context) {
        if (!(context instanceof PlayShiroSubjectContext)) {
            return super.createSubject(context);
        }

        PlayShiroSubjectContext sc = (PlayShiroSubjectContext) context;

        SecurityManager securityManager = sc.resolveSecurityManager();
        Session session = sc.resolveSession();
        boolean sessionEnabled = sc.isSessionCreationEnabled();
        PrincipalCollection principals = sc.resolvePrincipals();
        boolean authenticated = sc.resolveAuthenticated();
        String host = sc.resolveHost();
        Http.Context httpContext = sc.resolveHttpContext();

        return new PlayShiroDelegatingSubject(principals, authenticated, host,
                session, sessionEnabled, httpContext, securityManager);
    }
}
